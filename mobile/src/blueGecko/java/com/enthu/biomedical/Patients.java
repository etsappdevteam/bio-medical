package com.enthu.biomedical;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.view.View;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.enthu.biomedical.R;

import java.io.File;
import java.io.FileOutputStream;

public class Patients extends AppCompatActivity {
    EditText pname,page,pweight,pheight,pmob;
    int pageHeight = 1120;
    int pagewidth = 792;

    // creating a bitmap variable
    // for storing our images
    private EditText myEditText;

    // constant code for runtime permissions
    private static final int PERMISSION_REQUEST_CODE = 200;

    Bitmap bmp, scaledbmp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);


        myEditText = findViewById(R.id.name);
        page = findViewById(R.id.age);
        pheight = findViewById(R.id.height);
        pweight = findViewById(R.id.weight);
        pmob = findViewById(R.id.mnum);
        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        scaledbmp = Bitmap.createScaledBitmap(bmp, 140, 140, false);
        ActivityCompat.requestPermissions(Patients.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);



    }


    public void createMyPDF(View view){

        PdfDocument myPdfDocument = new PdfDocument();
        PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(pagewidth, pageHeight, 1).create();
        PdfDocument.Page myPage = myPdfDocument.startPage(myPageInfo);
        Paint paint = new Paint();
        Paint title = new Paint();
        Canvas canvas = myPage.getCanvas();
        Paint myPaint = new Paint();
        String dummy = "";
        String tittle = "Patient Test Details";
        String dummy1 = "";
        String myString  = "   Patient Name   :" +  myEditText.getText().toString();
        String myString1 = "    Patient Age   :" +  page.getText().toString();
        String myString2 = " Patient Height   :" +  pheight.getText().toString();
        String myString3 = " Patient Weight   :" +  pweight.getText().toString();
        String myString4 = " Contact Number   :" +  pmob.getText().toString();
        String dummy2 = "";
//        String moment    = "  Moment Status   :" + dataSMS.getText().toString();
//        String emotion   = "Emotional Level   :" + dataBattery.getText().toString();
        canvas.drawBitmap(scaledbmp, 56, 40, paint);

        // below line is used for adding typeface for
        // our text which we will be adding in our PDF file.
        title.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));

        // below line is used for setting text size
        // which we will be displaying in our PDF file.
        title.setTextSize(20);

        // below line is sued for setting color
        // of our text inside our PDF file.
        title.setColor(ContextCompat.getColor(this, R.color.red));

        canvas.drawText("Enthu Technology Solutions", 209, 80, title);
        canvas.drawText("India Pvt Ltd", 209, 140, title);

        int x =396, y=500;
        for (String line:dummy.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:dummy1.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:tittle.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString1.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString2.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString3.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString4.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:dummy2.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
//        for (String line:moment.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:emotion.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }



        myPdfDocument.finishPage(myPage);

        String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/Bio.pdf";
        File myFile = new File(myFilePath);
        try {
            myPdfDocument.writeTo(new FileOutputStream(myFile));
            Toast.makeText(Patients.this, "PDF file generated succesfully.", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e){
            e.printStackTrace();
            myEditText.setText("ERROR");
        }

        myPdfDocument.close();
    }

}

