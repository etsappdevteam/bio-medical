package com.enthu.biomedical;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.enthu.biomedical.activity.BrowserActivity;
import com.enthu.biomedical.activity.DeviceServicesActivity;
import com.enthu.biomedical.ble.BlueToothService;
import com.enthu.biomedical.ble.TimeoutGattCallback;
import com.enthu.biomedical.bluetoothdatamodel.datatypes.Characteristic;
import com.enthu.biomedical.bluetoothdatamodel.datatypes.Descriptor;
import com.enthu.biomedical.bluetoothdatamodel.datatypes.Service;
import com.enthu.biomedical.bluetoothdatamodel.parsing.Common;
import com.enthu.biomedical.bluetoothdatamodel.parsing.Converters;
import com.enthu.biomedical.bluetoothdatamodel.parsing.Engine;
import com.enthu.biomedical.log.TimeoutLog;
import com.enthu.biomedical.services.BluetoothLeService;
import com.enthu.biomedical.services.Write;
import com.enthu.biomedical.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


///////////////////////////////////line chart ///////////////////////////////////


public class Dashboard extends AppCompatActivity {
    private BluetoothGattCharacteristic mBluetoothCharact;
    public static UUID ota_service = UUID.fromString("1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0");
    private UUID ota_control = UUID.fromString("f7bf3564-fb6d-4e53-88a4-5e37e0326063");

    public final static String MESSAGE_KEY ="ganeshannt.senddata.message_key";
    public final static String MESSAGE_KEY1 ="ganeshannt.senddata.message_key1";

    private BlueToothService.Binding bluetoothBinding;
    private BluetoothAdapter mBluetoothAdapter;
    private BlueToothService service;
    TextView dataSMS, dataBattery, textbt, sms_value,smstext,battery_value;
    EditText pname,page,pweight,pheight,pmob;
    CardView sms, battery;
    Integer data1, data2,j=0;
    Integer blue,Green,yellow, red;
    DeviceServicesActivity deviceServicesActivity;
    BluetoothGattCharacteristic soilMoistureLevel,batteryLevel;
    BluetoothGattService mGattService;
    BluetoothLeService blueToothService;
    BluetoothGatt bluetoothGatt;
    Button Refresh;
    private String deviceAddress;
    Handler handler;
    private boolean serviceHasBeenSet;
    Integer SMSVALUE;
    ImageView emoji,gif;

//    // variables for our buttons.
//    Button generatePDFbtn;
//
//    // declaring width and height
//    // for our PDF file.
//    int pageHeight = 1120;
//    int pagewidth = 792;
//
//    // creating a bitmap variable
//    // for storing our images
//    private EditText myEditText;
//
//    // constant code for runtime permissions
//    private static final int PERMISSION_REQUEST_CODE = 200;
//
//
//    Bitmap bmp, scaledbmp;



    public void createMyPDF(View v) {

        Intent i = new Intent();
        i.setClass(this, Details.class);
        dataSMS = findViewById(R.id.sms_value);
        dataBattery = findViewById(R.id.battery_value);
        String moment = dataSMS.getText().toString();

        String emotion = dataBattery.getText().toString();


        i.putExtra(MESSAGE_KEY,moment);
        i.putExtra(MESSAGE_KEY1,emotion);
        startActivity(i);
    }

//    public void chart(View v) {
//
//        Intent i = new Intent();
//        i.setClass(this, Widget.class);
//        startActivity(i);
//    }


    int i=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
//        BarChart chart = findViewById(R.id.barchart);
//
//        ArrayList NoOfEmp = new ArrayList();
//
//        NoOfEmp.add(new BarEntry(945f, 0));
//        NoOfEmp.add(new BarEntry(1040f, 1));
//        NoOfEmp.add(new BarEntry(1133f, 2));
//        NoOfEmp.add(new BarEntry(1240f, 3));
//        NoOfEmp.add(new BarEntry(1369f, 4));
//        NoOfEmp.add(new BarEntry(1487f, 5));
//        NoOfEmp.add(new BarEntry(1501f, 6));
//        NoOfEmp.add(new BarEntry(1645f, 7));
//        NoOfEmp.add(new BarEntry(1578f, 8));
//        NoOfEmp.add(new BarEntry(1695f, 9));
//
//        ArrayList year = new ArrayList();
//
//        year.add("2008");
//        year.add("2009");
//        year.add("2010");
//        year.add("2011");
//        year.add("2012");
//        year.add("2013");
//        year.add("2014");
//        year.add("2015");
//        year.add("2016");
//        year.add("2017");
//
//        BarDataSet bardataset = new BarDataSet(NoOfEmp, "No Of Employee");
//        chart.animateY(5000);
//        BarData data = new BarData((IBarDataSet) year, bardataset);
//        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
//        chart.setData(data);

//        myEditText = findViewById(R.id.name);
//        page = findViewById(R.id.age);
//        pheight = findViewById(R.id.height);
//        pweight = findViewById(R.id.weight);
//        pmob = findViewById(R.id.mnum);
//        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
//        scaledbmp = Bitmap.createScaledBitmap(bmp, 140, 140, false);
//        ActivityCompat.requestPermissions(Dashboard.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);


        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Not supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        data1 = 1000;
        data2 = 3100;
        dataSMS = (TextView) findViewById(R.id.sms_value);
        dataBattery = findViewById(R.id.battery_value);
//        smstext = findViewById(R.id.smstxt);
        emoji = findViewById(R.id.emoji);
        sms = findViewById(R.id.sms);
        battery = findViewById(R.id.battery);
         gif = findViewById(R.id.gif);
//        Glide.with(this).load(R.drawable.mdt).into(gif);

//        page = findViewById(R.id.age);
//        pheight = findViewById(R.id.height);
//        pweight = findViewById(R.id.weight);
//        pmob = findViewById(R.id.mnum);



        initDevice(getDeviceAddress(savedInstanceState));


}



    public void readdata() {

        Thread t = new Thread() {
            int i = 0;

            @Override
            public void run() {

//                        bluetoothGatt.readCharacteristic(batteryLevel);
                while (!isInterrupted()) {

                    try {

                        Thread.sleep(100);

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {



                                    bluetoothGatt.readCharacteristic(soilMoistureLevel);


                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }

        };

        t.start();

        Thread t2 = new Thread() {


            @Override
            public void run() {
//                        bluetoothGatt.readCharacteristic(batteryLevel);
                while (!isInterrupted()) {

                    try {
                        Thread.sleep(200);  //1000ms = 1 sec

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                bluetoothGatt.readCharacteristic(batteryLevel);

                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        t2.start();
    }

    private void colorSmsAction(int data1) {


        if (data1 == 44) {

            dataSMS.setText("Movement Detected");
            dataSMS.setTextColor(Color.BLACK);
            gif.setBackgroundResource(R.drawable.run);

        }
         else {

            dataSMS.setText("Movement Not Detected");
            gif.setBackgroundResource(R.drawable.mt3);
        }
    }
    public void colorbatteryAction(int data2){
        dataBattery.setText(Integer.toString(data2));

        if (data2 < 60) {
            emoji.setBackgroundResource(R.drawable.sad);
            dataBattery.setTextColor(Color.BLACK);
        } else if (data2 < 130) {
            emoji.setBackgroundResource(R.drawable.happy);
            dataBattery.setTextColor(Color.BLACK);
        } else if (data2 < 170) {
            emoji.setBackgroundResource(R.drawable.normal);
            dataBattery.setTextColor(Color.BLACK);

        } else if (data2 < 400) {
            emoji.setBackgroundResource(R.drawable.stress);
            dataBattery.setTextColor(Color.BLACK);
        }
          else if (data2 < 600) {
            dataBattery.setText("Device Not Connected");
            emoji.setBackgroundResource(R.drawable.rounded_button_white);
            dataBattery.setTextColor(Color.BLACK);
        }
          else {
//            dataBattery.setText("Not Connected Correctly");
//            emoji.setBackgroundColor(Color.parseColor("#FFFFF"));
//            dataBattery.setTextColor(Color.BLACK);
        }

    }


    // getting the device Address through the previous activity Selected Device
    private String getDeviceAddress(final Bundle savedInstanceState) {
        String deviceAddress;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            System.out.println(extras);
            if (extras == null) {
                deviceAddress = null;
            } else {
                deviceAddress = extras.getString("DEVICE_SELECTED_ADDRESS");
            }
        } else {
            deviceAddress = savedInstanceState.getString("DEVICE_SELECTED_ADDRESS");
        }
        this.deviceAddress = deviceAddress;
        System.out.println(mBluetoothAdapter.getRemoteDevice(deviceAddress));
        initDevice(deviceAddress);
   return  deviceAddress;

    }


    ///====================================================================
    //======================Load characterstics =================================
    private void loadCharacteristicDescriptors(BluetoothGattCharacteristic bluetoothGattCharacteristic)
    {
        for (BluetoothGattDescriptor d : bluetoothGattCharacteristic.getDescriptors()) {
            Descriptor descriptor = Engine.getInstance().getDescriptorByUUID(d.getUuid());


            if (descriptor == null) {

            } else {
                descriptor.getName();
                descriptor.getName();
            }


        }
    }


    //===================================================================





    //===============================================================================
    //=====================INIT DEVICE ==============================================
    private void initServicesViews() {

        // iterate through all of the services for the device, inflate and add views to the scrollview
        ArrayList<BluetoothGattService> services = (ArrayList<BluetoothGattService>) bluetoothGatt.getServices(); //service.getConnectedGatt().getServices();
        for (int position = 0; position < services.size(); position++) {


            // get information about service at index 'position'
            UUID uuid = services.get(position).getUuid();
            Service service = Engine.getInstance().getService(uuid);
            String serviceName = Common.getServiceName(uuid, getApplicationContext());
             String serviceUuid = Common.getUuidText(uuid);

            serviceName = Common.checkOTAService(serviceUuid, serviceName);

            // initialize information about services in service item container
            System.out.println(serviceUuid);


            // initialize views for each characteristic of the service, put into characteristics expansion for service's list item
            final BluetoothGattService blueToothGattService = service == null ? services.get(position) : bluetoothGatt.getService(service.getUuid());
            List<BluetoothGattCharacteristic> characteristics = blueToothGattService.getCharacteristics();
            if (characteristics.size() == 0) {

                continue;
            }
            // iterate through the characteristics of this service
            for (final BluetoothGattCharacteristic bluetoothGattCharacteristic : characteristics) {
                // retrieve relevant bluetooth data for characteristic of service
                final BluetoothGattCharacteristic thisCharacteristic = bluetoothGattCharacteristic;
                // the engine parses through the data of the btgattcharac and returns a wrapper characteristic
                // the wrapper characteristic is matched with accepted bt gatt profiles, provides field types/values/units
                System.out.println(bluetoothGattCharacteristic.getUuid().toString());

                Characteristic charact = Engine.getInstance().getCharacteristic(bluetoothGattCharacteristic.getUuid());
                String characteristicName;

                if (charact != null) {
                    characteristicName = charact.getName().trim();
                } else {
                    characteristicName =bluetoothGattCharacteristic.getUuid().toString();
                }


                final String characteristicUuid = (charact != null ? Common.getUuidText(charact.getUuid()) : Common.getUuidText(bluetoothGattCharacteristic.getUuid()));

                //TODO: They are in GattCharacteristic, but their names are not appearing




                loadCharacteristicDescriptors(bluetoothGattCharacteristic);

                // init/populate ui elements with info from bluetooth data for characteristic of service

                System.out.println(characteristicName);
                if("00002a00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    soilMoistureLevel =bluetoothGattCharacteristic;

                    System.out.println( bluetoothGattCharacteristic.getValue());
                }
                if("00003a00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    batteryLevel=bluetoothGattCharacteristic;
                }
                readdata();
//                if("00001b00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
//                    Write.smsDryThershold=bluetoothGattCharacteristic;
//                }
//                if("00001c00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
//                Write.smsWetThershold=bluetoothGattCharacteristic;
//                }
//                if("00001d00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
//                    Write.smsMediumThershold=bluetoothGattCharacteristic;
//                }
//                if("00002b00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
//                    Write.batHighThershold=bluetoothGattCharacteristic;
//                }
//                if("00002c00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
//                    Write.batLowThershold=bluetoothGattCharacteristic;
//                }


System.out.println(characteristicUuid);



            }
        }
    }

    //===============================================================================



    private void initDevice(final String deviceAddress) {
        handler = new Handler();

        bluetoothBinding = new BlueToothService.Binding(this) {
            @Override
            protected void onBound(BlueToothService service) {//todo dubel
                serviceHasBeenSet = true;
final BluetoothDevice device =mBluetoothAdapter.getRemoteDevice(deviceAddress);
                Dashboard.this.service = service;
                service.connectGatt(device,false,gattCallback);
                if (!service.isGattConnected(deviceAddress)) {
                    Toast.makeText(Dashboard.this, R.string.toast_debug_connection_failed, Toast.LENGTH_LONG).show();

                } else {
                    BluetoothGatt bG = service.getConnectedGatt(deviceAddress);
                    System.out.println(bG.getDevice());

                    if (bG == null) {
                        Toast.makeText(Dashboard.this, R.string.device_not_from_EFR, Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                    service.registerGattCallback(true, gattCallback);
                    System.out.println(bG.getServices());
                    System.out.println(bG.getService(UUID.fromString("00002a00-0000-1000-8000-00805F9B34FB")));
                    if (bG.getServices() != null && !bG.getServices().isEmpty()) {
                        bluetoothGatt = bG;
                        System.out.println(bluetoothGatt);
                        initServicesViews();

                    } else {

                        bG.discoverServices();
                        Write.gattService=bG;


                    }
                }
            }
        };
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        BlueToothService.bind(bluetoothBinding);
                    }
                });
            }
        }, 0);

    }

    /**
     * BLUETOOTH GATT CALLBACKS
     *********************************************************/
    private TimeoutGattCallback gattCallback = new TimeoutGattCallback() {
        @Override
        public void onReadRemoteRssi(final BluetoothGatt gatt, final int rssi, int status) {

                super.onReadRemoteRssi(gatt, rssi, status);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("onReadRemoteRssi", "RSSI: " + rssi);

                    }
                });

        }

        @Override
        public void onTimeout() {
            Constants.LOGS.add(new TimeoutLog());
            super.onTimeout();
            Log.d("gattCallback", "onTimeout");
        }


//OnConnectionState Change
// It can be either connected or disconnected state
@Override
public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
    super.onConnectionStateChange(gatt, status, newState);
    runOnUiThread(new Runnable() {
        @Override
        public void run() {
//            Toast.makeText(Dashboard.this,"Device timeout",Toast.LENGTH_LONG).show();
            Intent i= new Intent(Dashboard.this,BrowserActivity.class);
            startActivity(i);
        }
    });
    Log.d("onConnectionStateChange", "status = " + status + " - newState = " + newState);

    Log.i("BLE service", "onConnectionStateChange - status: " + status + " - new state: " + newState);




}



        @Override //CALLBACK ON CHARACTERISTIC READ
        public void onCharacteristicRead(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
              if("00002a00-0000-1000-8000-00805f9b34fb".equals(characteristic.getUuid().toString())){
                data1= Integer.valueOf(Converters.getDecimalValue1(characteristic.getValue()));
//                SMSVALUE=data1;
//                System.out.println(data1);
//            dataSMS.setText(Integer.toString(data1));
//                colorSmsAction(data1);
                colorSmsAction(data1);
            }

            if("00003a00-0000-1000-8000-00805f9b34fb".equals(characteristic.getUuid().toString())){
                data2=Integer.valueOf(Converters.getDecimalValue1(characteristic.getValue()));

                colorbatteryAction(data2);
            }



            }


        @Override //CALLBACK ON CHARACTERISTIC WRITE (PROPERTY: WHITE)
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {


            if (characteristic.getValue().length < 10)

                Log.d("OnCharacteristicRead", "Char: " + characteristic.getUuid().toString() + " Value: " + Converters.getHexValue(characteristic.getValue()) + " Status: " + status);

            if (status != 0) { // Error Handling
                Log.d("onCharWrite", "status: " + Integer.toHexString(status));
                final int error = status;
            }

            Toast.makeText(getApplicationContext(),"Updated Successfully",Toast.LENGTH_LONG).show();



           /// bluetoothGatt.readCharacteristic(characteristic);
        }

        @Override //CALLBACK ON DESCRIPTOR WRITE
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
//            if (currentWriteReadFragment != null) {
//                currentWriteReadFragment.onDescriptorWrite(descriptor.getUuid());
//            }
        }

        @Override //CALLBACK ON DESCRIPTOR READ
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {



                byte[] value = new byte[1];
                value[0] = (byte) 0;
//                value[1] = (byte) 0xFF;

                if (descriptor.getValue()[0] == value[0] ) {

                    Log.i("descriptor", "getValue " + Converters.getHexValue(descriptor.getValue()));


                }
            }


        @Override //CALLBACK ON CHARACTERISTIC CHANGED VALUE (READ - CHARACTERISTIC NOTIFICATION)
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

//            for (int key : characteristicFragments.keySet()) {
//                FragmentCharacteristicDetail fragment = characteristicFragments.get(key);
//                if (fragment != null && fragment.getmCharact().getUuid().equals(characteristic.getUuid())) {
//                    fragment.onActionDataAvailable(characteristic.getUuid().toString());
//                    break;
//                }
//            }

        }

        @Override //CALLBACK ON SERVICES DISCOVERED
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
//            bluetoothGatt = gatt;

            if (bluetoothGatt != gatt) {
                bluetoothGatt = gatt;

            }
            initServicesViews();
            getServicesInfo(gatt);

        }
    };
    /************************************************************************************/


    public BluetoothGatt getBluetoothGatt() {
        return bluetoothGatt;
    }


    public void getServicesInfo(BluetoothGatt gatt) {

        List<BluetoothGattService> gattServices = gatt.getServices();
        Log.i("onServicesDiscovered", "Services count: " + gattServices.size());

        for (BluetoothGattService gattService : gattServices) {
            String serviceUUID = gattService.getUuid().toString();


            Log.i("onServicesDiscovered", "Service UUID " + serviceUUID + " - Char count: " + gattService.getCharacteristics().size());
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();

            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {

                String CharacteristicUUID = gattCharacteristic.getUuid().toString();
                Log.i("onServicesDiscovered", "Characteristic UUID " + CharacteristicUUID + " - Properties: " + gattCharacteristic.getProperties());


            }
        }
    }


//    public void createMyPDF(View view){
//
//        PdfDocument myPdfDocument = new PdfDocument();
//        PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(pagewidth, pageHeight, 1).create();
//        PdfDocument.Page myPage = myPdfDocument.startPage(myPageInfo);
//        Paint paint = new Paint();
//        Paint title = new Paint();
//        Canvas canvas = myPage.getCanvas();
//        Paint myPaint = new Paint();
//        String dummy = "";
//        String tittle = "Patient Test Details";
//        String dummy1 = "";
//        String myString  = "   Patient Name   :" +  myEditText.getText().toString();
//        String myString1 = "    Patient Age   :" +  page.getText().toString();
//        String myString2 = " Patient Height   :" +  pheight.getText().toString();
//        String myString3 = " Patient Weight   :" +  pweight.getText().toString();
//        String myString4 = " Contact Number   :" +  pmob.getText().toString();
//        String dummy2 = "";
//        String moment    = "  Moment Status   :" + dataSMS.getText().toString();
//        String emotion   = "Emotional Level   :" + dataBattery.getText().toString();
//        canvas.drawBitmap(scaledbmp, 56, 40, paint);
//
//        // below line is used for adding typeface for
//        // our text which we will be adding in our PDF file.
//        title.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
//
//        // below line is used for setting text size
//        // which we will be displaying in our PDF file.
//        title.setTextSize(20);
//
//        // below line is sued for setting color
//        // of our text inside our PDF file.
//        title.setColor(ContextCompat.getColor(this, R.color.red));
//
//        canvas.drawText("Enthu Technology Solutions", 209, 80, title);
//        canvas.drawText("India Pvt Ltd", 209, 140, title);
//
//        int x =396, y=500;
//        for (String line:dummy.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:dummy1.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:tittle.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:myString.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:myString1.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:myString2.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:myString3.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:myString4.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:dummy2.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:moment.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//        for (String line:emotion.split("\n")){
//            myPage.getCanvas().drawText(line, x, y, myPaint);
//            myPaint.setTextAlign(Paint.Align.CENTER);
//            myPaint.setTextSize(30);
//            y+=myPaint.descent()-myPaint.ascent();
//        }
//
//
//
//        myPdfDocument.finishPage(myPage);
//
//        String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/Bio.pdf";
//        File myFile = new File(myFilePath);
//        try {
//            myPdfDocument.writeTo(new FileOutputStream(myFile));
//            Toast.makeText(Dashboard.this, "PDF file generated succesfully.", Toast.LENGTH_SHORT).show();
//        }
//        catch (Exception e){
//            e.printStackTrace();
//            myEditText.setText("ERROR");
//        }
//
//        myPdfDocument.close();
//    }


}












