package com.enthu.biomedical.interfaces;

import com.enthu.biomedical.ble.BluetoothDeviceInfo;

public interface FindKeyFobCallback {
    void findKeyFob(BluetoothDeviceInfo fob);

    void triggerDisconnect();

    String getDeviceName();
}
