package com.enthu.biomedical.utils;

public enum ToolbarName {
    CONNECTIONS, LOGS, FILTER
}
