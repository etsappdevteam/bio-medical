package com.enthu.biomedical;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.view.View;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import java.io.FileOutputStream;

public class Details extends AppCompatActivity {
    EditText pname,page,pweight,pheight,pmob,address;
    TextView emo,mot;
    int pageHeight = 1120;
    int pagewidth = 792;
    public final static String MESSAGE_KEY ="ganeshannt.senddata.message_key";
    public final static String MESSAGE_KEY1 ="ganeshannt.senddata.message_key1";
    // creating a bitmap variable
    // for storing our images
    private EditText myEditText;

    // constant code for runtime permissions
    private static final int PERMISSION_REQUEST_CODE = 200;

    Bitmap bmp, scaledbmp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        myEditText = findViewById(R.id.name);
        page = findViewById(R.id.age);
        pheight = findViewById(R.id.height);
        pweight = findViewById(R.id.weight);
        pmob = findViewById(R.id.mnum);
        address = findViewById(R.id.addr);
        mot = findViewById(R.id.moment);
        emo = findViewById(R.id.emotion);
        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        scaledbmp = Bitmap.createScaledBitmap(bmp, 140, 140, false);
        ActivityCompat.requestPermissions(Details.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intent = getIntent();
        String moment = intent.getStringExtra(MESSAGE_KEY);
        String emotion = intent.getStringExtra(MESSAGE_KEY1);
        mot.setText(moment);
        emo.setText(emotion);


    }



//    public void share(View v) {
//
//        File outputFile = new File(Environment.getExternalStoragePublicDirectory
//                (Environment.DIRECTORY_DOWNLOADS), "example.pdf");
//        Uri uri = Uri.fromFile(outputFile);
//
//        Intent share = new Intent();
//        share.setAction(Intent.ACTION_SEND);
//        share.setType("application/pdf");
//        share.putExtra(Intent.EXTRA_STREAM, uri);
//        share.setPackage("com.whatsapp");
//
//        Details.startActivity(share);
//    }
//
    public void savePdf(View view){



        PdfDocument myPdfDocument = new PdfDocument();


        PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(pagewidth, pageHeight, 1).create();
        PdfDocument.Page myPage = myPdfDocument.startPage(myPageInfo);
        Paint paint = new Paint();
        Paint title = new Paint();
        Canvas canvas = myPage.getCanvas();
        Paint myPaint = new Paint();
        String pdf  = myEditText.getText().toString();
        String dummy = "";
        String tittle = "Patient Test Details";
        String dummy1 = "";
        String myString  = " Patient Name     :" +  myEditText.getText().toString();
        String myString1 = " Patient Age      :" +  page.getText().toString();
        String myString2 = " Patient Height   :" +  pheight.getText().toString()+"Cm";
        String myString3 = " Patient Weight   :" +  pweight.getText().toString()+"Kg";
        String myString4 = " Contact Number   :" +  pmob.getText().toString();
        String myString5 = " Address          :" +  address.getText().toString();
        String myString6 = " Movement Status  :" +  mot.getText().toString();
        String myString7=  " Emotional Level  :" +  emo.getText().toString();
        String dummy2 = "";








//        String moment    = "  Moment Status   :" + dataSMS.getText().toString();
//        String emotion   = "Emotional Level   :" + dataBattery.getText().toString();
        canvas.drawBitmap(scaledbmp, 56, 40, paint);

        // below line is used for adding typeface for
        // our text which we will be adding in our PDF file.
        title.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));

        // below line is used for setting text size
        // which we will be displaying in our PDF file.
        title.setTextSize(20);




        // below line is sued for setting color
        // of our text inside our PDF file.
        title.setColor(ContextCompat.getColor(this, R.color.blue));

        canvas.drawText("Enthu Technology Solutions", 209, 80, title);
        canvas.drawText("India Pvt Ltd", 209, 140, title);

        int x =396, y=500;
        for (String line:dummy.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:dummy1.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);

            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:tittle.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString1.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }

        for (String line:myString2.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString3.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString4.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString5.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();

        }
        for (String line:myString6.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:myString7.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }
        for (String line:dummy2.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            myPaint.setTextAlign(Paint.Align.CENTER);
            myPaint.setTextSize(30);
            y+=myPaint.descent()-myPaint.ascent();
        }




        myPdfDocument.finishPage(myPage);

        String myFile= Environment.getExternalStorageDirectory()+"/"+pdf+".pdf";
        try {
            myPdfDocument.writeTo(new FileOutputStream(myFile));
//            if (!myFile.exists()){
//                Toast.makeText(this, "File doesn't exists", Toast.LENGTH_LONG).show();
//                return;
//            }

        }
        catch (Exception e){
            e.printStackTrace();
            myEditText.setText("ERROR");
        }

        /////VAlidation

        final String Name=myEditText.getText().toString();
        final String word=page.getText().toString();
        final String ht=pheight.getText().toString();
        final String wt=pweight.getText().toString();
        final String mb=pmob.getText().toString();
        final String add=address.getText().toString();
        if(Name.length()==0)
        {
            myEditText.requestFocus();
            myEditText.setError("FIELD CANNOT BE EMPTY");
        }
        else if(word.length()==0)
        {
            page.requestFocus();
            page.setError("FIELD CANNOT BE EMPTY");
        }
        else if(ht.length()==0)
        {
            pheight.requestFocus();
            pheight.setError("FIELD CANNOT BE EMPTY");
        }
        else if(wt.length()==0)
        {
            pweight.requestFocus();
            pweight.setError("FIELD CANNOT BE EMPTY");
        }
        else if(mb.length()==0)
        {
            pmob.requestFocus();
            pmob.setError("FIELD CANNOT BE EMPTY");
        }
        else if(add.length()==0)
        {
            address.requestFocus();
            address.setError("FIELD CANNOT BE EMPTY");
        }
        else
        {

            Intent intentShare = new Intent(Intent.ACTION_SEND);
            intentShare.setType("application/pdf");
            intentShare.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+myFile));
            startActivity(Intent.createChooser(intentShare, "Share the file ..."));
            Toast.makeText(Details.this, "PDF file generated succesfully.", Toast.LENGTH_SHORT).show();
        }

        myPdfDocument.close();
    }
//    public void share(View view){
//        File file = new File(stringFile);
//        if (!file.exists()){
//            Toast.makeText(this, "File doesn't exists", Toast.LENGTH_LONG).show();
//            return;
//        }
//        Intent intentShare = new Intent(Intent.ACTION_SEND);
//        intentShare.setType("application/pdf");
//        intentShare.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+file));
//        startActivity(Intent.createChooser(intentShare, "Share the file ..."));
//    }





}
