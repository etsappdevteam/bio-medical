package com.enthu.biomedical.mappings;

public enum MappingType {
    SERVICE,
    CHARACTERISTIC
}
