package com.enthu.biomedical.ble;

import android.bluetooth.BluetoothGattCallback;

public abstract class TimeoutGattCallback extends BluetoothGattCallback {
    public void onTimeout() {
    }
}
