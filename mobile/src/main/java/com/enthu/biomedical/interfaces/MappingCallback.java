package com.enthu.biomedical.interfaces;

import com.enthu.biomedical.mappings.Mapping;

public interface MappingCallback {
    void onNameChanged(Mapping mapping);
}
