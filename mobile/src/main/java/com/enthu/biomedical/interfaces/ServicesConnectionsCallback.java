package com.enthu.biomedical.interfaces;

import com.enthu.biomedical.ble.BluetoothDeviceInfo;

public interface ServicesConnectionsCallback {
    void onDisconnectClicked(BluetoothDeviceInfo deviceInfo);

    void onDeviceClicked(BluetoothDeviceInfo device);
}
