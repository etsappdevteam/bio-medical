package com.enthu.biomedical;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.enthu.biomedical.services.Write;

import java.util.Arrays;

import butterknife.InjectView;

public class Dropdown extends AppCompatActivity {

    Spinner spinner_d;
    @InjectView(R.id.help_button)
    TextView helpButton;

    private Dialog helpDialog;
    private Dialog hiddenDebugDialog;
    private String DEVICE_ADDRESS;
    private Dialog alertDialogView;
    private TextView save,save1,save2,save3;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String SANDYWETDATASAVE = "text";
    public static final String SANDYMEDIUMWETDATASAVE = "text1";
    public static final String SANDYMEDIUMDRYDATASAVE = "text2";

    public static final String SHARED_PREFS01 = "sharedPrefs01";
    public static final String CLAYWETDATASAVE = "texts1";
    public static final String CLAYMEDIUMWETDATASAVE = "texts2";
    public static final String CLAYMEDIUMDRYDATASAVE = "texts3";

    public static final String SHARED_PREFS02 = "sharedPrefs02";
    public static final String SILTWETDATASAVE = "texts01";
    public static final String SILTMEDIUMWETDATASAVE = "texts02";
    public static final String SILTMEDIUMDRYDATASAVE = "texts03";


    private String sandywetdatasave;
    private String sandymediumwetdatasave;
    private String sandymediumdrydatasave;;


    private String claywetdatasave;
    private String claymediumwetdatasave;
    private String claymediumdrydatasave;

    private String siltwetdatasave;
    private String siltmediumwetdatasave;
    private String siltmediumdrydatasave;



String type[]={"SANDY","CLAY","SILT"};
EditText SandymediumEdit,SandywetEdit,SandydryEdit,ClaywetEdit,ClaymediumEdit,ClaydryEdit,SiltwetEdit,SiltdryEdit,SiltmediumEdit;
Button sandywet,sandydefault,sandymedium,sandydry,claywet,claydefault,claymedium,siltwet,siltdefault,siltmedium;
    EditText batLowtext,textHigh,textSmsDry,textSmsWet,textSmsMedium;
    Button btn,battLow,bttnHigh,bttnsmsWet,bttsmsDry,bttsmsMedium,btnhelp;
    TextView soilsandywet,soilsandymediumwet,soilsandymediumdry,soilclaywet,soilclaymediumwet,soilclaymediumdry,soilsiltwet,soilsiltmediumwet,soilsiltmediumdry,Sandywetcolor,Sandymediumcolor,Sandydrycolor;
    TextView blue,green,yellow;
    BluetoothGatt gattService;
 ArrayAdapter<String>arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropdown);

        findViewById(R.id.buttonShowDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //calling this method to show our android custom alert dialog
                showCustomDialog();
            }
        });

        battLow =findViewById(R.id.batLowThersholdUpdate);
        bttnHigh=findViewById(R.id.batHighThersholdUpdate);
        LinearLayout save = findViewById(R.id.soildata);
        LinearLayout save1 = findViewById(R.id.soildata1);
        LinearLayout save2 = findViewById(R.id.soildata2);

//        blue = findViewById(R.id.clrblue);
//        green = findViewById(R.id.clrgreen);
//        yellow= findViewById(R.id.clryellow);

//        save3 = (TextView) findViewById(R.id.soiltype);
        batLowtext=findViewById(R.id.batteryLow);
        textHigh=findViewById(R.id.batteryHigh);
        //===============================================
        gattService= Write.gattService;

        spinner_d=(Spinner)findViewById(R.id.spinner_dropdown);
        arrayAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,type);
        spinner_d.setAdapter(arrayAdapter);
//        RelativeLayout batteryLayout = findViewById(R.id.batteryContainer);
//        batteryLayout.setVisibility(View.INVISIBLE);


        //Write BluetoothGatt Service



        spinner_d.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),"You are Selected:" +type[i],Toast.LENGTH_SHORT).show();

                Write.colorTypes=type[i];

                String stype = type[i];
                LinearLayout layout = findViewById(R.id.container);
                LinearLayout layoutMedium = findViewById(R.id.container1);
                LinearLayout layoutDry = findViewById(R.id.container2);

                LinearLayout save = findViewById(R.id.soildata);
                LinearLayout save1 = findViewById(R.id.soildata1);
                LinearLayout save2 = findViewById(R.id.soildata2);

                LinearLayout layoutbtn = findViewById(R.id.btnwet);
                LinearLayout layoutMediumbtn = findViewById(R.id.btndefault);
//                LinearLayout layoutDrybtn = findViewById(R.id.btndry);

                layout.setVisibility(View.INVISIBLE);
                layoutMedium.setVisibility(View.INVISIBLE);
                layoutDry.setVisibility(View.INVISIBLE);

                save.setVisibility(View.INVISIBLE);
                save1.setVisibility(View.INVISIBLE);
                save2.setVisibility(View.INVISIBLE);



                layoutbtn.setVisibility(View.INVISIBLE);
                layoutMediumbtn.setVisibility(View.INVISIBLE);
//                layoutDrybtn.setVisibility(View.INVISIBLE);

                layout.removeAllViews();
                layoutMedium.removeAllViews();
                layoutDry.removeAllViews();

                save.removeAllViews();
                save1.removeAllViews();
                save2.removeAllViews();

                layoutbtn.removeAllViews();
                layoutMediumbtn.removeAllViews();
//                layoutDrybtn.removeAllViews();



                LinearLayout.LayoutParams wetbtn = new LinearLayout.LayoutParams(400,LinearLayout.LayoutParams.WRAP_CONTENT);
                wetbtn.setMarginStart(120);
                wetbtn.setMarginEnd(200);



                LinearLayout.LayoutParams defaultbtn = new LinearLayout.LayoutParams(250,LinearLayout.LayoutParams.WRAP_CONTENT);
                defaultbtn.gravity = Gravity.RIGHT;
                defaultbtn.setMarginEnd(50);



//                LinearLayout.LayoutParams mediumbtn = new LinearLayout.LayoutParams(190,95);
//                mediumbtn.gravity = Gravity.RIGHT;
//                mediumbtn.setMarginEnd(100);
//
//                LinearLayout.LayoutParams drybtn = new LinearLayout.LayoutParams(190,95);
//                drybtn.gravity = Gravity.RIGHT;
//                drybtn.setMarginEnd(100);


                if("SANDY"==type[i]){

                    LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(900, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp1.setLayoutDirection(LinearLayout.VERTICAL);
                    lp1.setMarginStart(50);

                    SandywetEdit= new EditText(getApplicationContext());
                    SandymediumEdit= new EditText(getApplicationContext());
                    SandydryEdit= new EditText(getApplicationContext());

                    Sandywetcolor= new TextView(getApplicationContext());
                    Sandymediumcolor= new TextView(getApplicationContext());
                    Sandydrycolor= new TextView(getApplicationContext());


                    soilsandywet= new TextView(getApplicationContext());
                    soilsandywet.setTypeface(null, Typeface.BOLD);
//                    soil1.setTextColor(Color.WHITE);
                    soilsandymediumwet= new TextView(getApplicationContext());
                    soilsandymediumwet.setTypeface(null, Typeface.BOLD);
//                    soil2.setTextColor(Color.WHITE);
                    soilsandymediumdry= new TextView(getApplicationContext());
                    soilsandymediumdry.setTypeface(null, Typeface.BOLD);
//                    soil3.setTextColor(Color.WHITE);


                    //=============================Global Value For all activity ===========================
                  Write.sandyWet =SandywetEdit.getText().toString();
                  Write.sandyMedium =SandymediumEdit.getText().toString();
                  Write.sandyDry =SandydryEdit.getText().toString();


                    SandywetEdit.setHint("Wet : 0 - 246 ");
                    SandymediumEdit.setHint("Medium Wet: 246 - 619");
                    SandydryEdit.setHint("Medium Dry : 619 - 1229 ");



                    sandywet= new Button(getApplicationContext());
                    sandywet.setBackgroundColor(R.drawable.square_button);
                    sandywet.setTextColor(Color.WHITE);

                    sandydefault= new Button(getApplicationContext());
                    sandydefault.setBackgroundColor(Color.parseColor("#40cf68"));
                    sandydefault.setTextColor(Color.WHITE);
//                    sandymedium= new Button(getApplicationContext());
//                    sandymedium.setBackgroundColor(Color.parseColor("#32CD32"));
//                    sandymedium.setTextColor(Color.WHITE);
//                    sandydry= new Button(getApplicationContext());
//                    sandydry.setBackgroundColor(Color.parseColor("#ffff66"));


//                    sandywet.setGravity(Gravity.RIGHT);
//                    sandymedium.setGravity(Gravity.RIGHT);
//                    sandydry.setGravity(Gravity.RIGHT);


                    sandywet.setLayoutParams(wetbtn);
                    sandydefault.setLayoutParams(defaultbtn);
//                    sandydry.setLayoutParams(drybtn);


                    sandywet.setText("Send");
                    sandydefault.setText("Default");
//                    sandydry.setText("send");



                    SandywetEdit.setLayoutParams(lp1);
                    SandymediumEdit.setLayoutParams(lp1);
                    SandydryEdit.setLayoutParams(lp1);

                    soilsandywet.setLayoutParams(lp1);
                    soilsandymediumwet.setLayoutParams(lp1);
                    soilsandymediumdry.setLayoutParams(lp1);

                    layout.addView(SandywetEdit);
                    layoutMedium.addView(SandymediumEdit);
                    layoutDry.addView(SandydryEdit);

                    save.addView(soilsandywet);
                    save1.addView(soilsandymediumwet);
                    save2.addView(soilsandymediumdry);

                    layoutbtn.addView(sandywet);
                    layoutMediumbtn.addView(sandydefault);
//                    layoutDrybtn.addView(sandydry);

                    layout.setVisibility(View.VISIBLE);
                    layoutMedium.setVisibility(View.VISIBLE);
                    layoutDry.setVisibility(View.VISIBLE);

                    save.setVisibility(View.VISIBLE);
                    save1.setVisibility(View.VISIBLE);
                    save2.setVisibility(View.VISIBLE);

                    layoutbtn.setVisibility(View.VISIBLE);
                    layoutMediumbtn.setVisibility(View.VISIBLE);
//                    layoutDrybtn.setVisibility(View.VISIBLE);



                    //========================== Button action For Write CharcterStics ========================
                        //==================== sandy wet ====================
                    sandywet.setOnClickListener(new View.OnClickListener(){

                        @Override
                        public void onClick(View v) {
                            sandywet.setBackgroundColor(Color.parseColor("#32CD32"));
                            String value= SandywetEdit.getText().toString();
                            soilsandywet.setText(SandywetEdit.getText().toString());

                            saveData();
//
                                if (value.length() <= 4 && !(value.length() == 0)) {

                                if (Integer.valueOf(value) <= 4095) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsWetThershold.setValue(newValue);
                                    SandywetEdit.setText("");

                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsWetThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsWetThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsWetThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsWetThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SandywetEdit.setError("Enter valid Thershold");
                                    sandywet.setBackgroundColor(Color.parseColor("#32CD32"));
                                }

                            }

                        else {
                            SandywetEdit.setError("Enter 4 digit Number ");
                        }
//                        }
//                    });

                    //==================== sandy Medium ====================
//                    sandymedium.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {




                            String value1= SandymediumEdit.getText().toString();
                            soilsandymediumwet.setText(SandymediumEdit.getText().toString());

                            saveData();

                            if(value1.length()<=4 && !(value1.length()==0)) {

                                if (Integer.valueOf(value1) <= 4095) {


                                    byte[] newValue1 = value1.getBytes();
                                    System.out.println(newValue1);

                                    System.out.println(Arrays.toString(newValue1));
                                    Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsMediumThershold.setValue(newValue1);
                                    SandymediumEdit.setText("");

                                    if (value1 != null) {

                                        byte[] newValue11 = value1.getBytes();

                                        try {
                                            Write.smsMediumThershold.setValue(newValue11);
                                            gattService.writeCharacteristic(Write.smsMediumThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsMediumThershold.setValue(value1);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SandymediumEdit.setError("Enter valid Thershold");

                                }
                            }
                            else {
                                SandymediumEdit.setError("Enter 4 digit Number ");
                            }
//                        }
//                    });

                    //==================== sandy Dry ====================
//                    sandydry.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                            String value2= SandydryEdit.getText().toString();
                            soilsandymediumdry.setText(SandydryEdit.getText().toString());

                            saveData();

                            if(value2.length()<=4 && !(value2.length()==0)) {

                                if (Integer.valueOf(value2) <= 4095) {


                                    byte[] newValue2 = value2.getBytes();
                                    System.out.println(newValue2);

                                    System.out.println(Arrays.toString(newValue2));
                                    Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsDryThershold.setValue(newValue2);
                                    SandydryEdit.setText("");

                                    if (value2 != null) {

                                        byte[] newValue12 = value2.getBytes();

                                        try {
                                            Write.smsDryThershold.setValue(newValue12);
                                            gattService.writeCharacteristic(Write.smsDryThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsDryThershold.setValue(value2);
                                        gattService.writeCharacteristic(Write.smsDryThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SandydryEdit.setError("Enter valid Thershold");

                                }
                            }
                            else {
                                SandydryEdit.setError("Enter 4 digit Number ");
                            }
                        }


                    });
                    loadData();
                    updateViews();
                    //==========================================================================//
                    checkValidation();




                ///Default value button

                sandydefault.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        sandydefault.setBackgroundColor(Color.parseColor("#00ccff"));
                        String value= SandywetEdit.getText().toString();
                        soilsandywet.setText("246");

                        saveData();
//
                                byte[] newValue = value.getBytes();
                                System.out.println(newValue);

                                System.out.println(Arrays.toString(newValue));
                                Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.smsWetThershold.setValue(newValue);

                                SandywetEdit.setText("");

                                if (value != null) {

                                    byte[] newValue1 = value.getBytes();

                                    try {
                                        Write.smsWetThershold.setValue(newValue1);
                                        gattService.writeCharacteristic(Write.smsWetThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.smsWetThershold.setValue("246");
                                    gattService.writeCharacteristic(Write.smsWetThershold);
                                }
                                Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();



//                        }
//                    });

                        //==================== sandy Medium ====================
//                    sandymedium.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {




                        String value1= SandymediumEdit.getText().toString();
                        soilsandymediumwet.setText("619");

                        saveData();




                                byte[] newValue1 = value1.getBytes();
                                System.out.println(newValue1);

                                System.out.println(Arrays.toString(newValue1));
                                Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.smsMediumThershold.setValue(newValue1);
                                SandymediumEdit.setText("");


                                if (value1 != null) {

                                    byte[] newValue11 = value1.getBytes();

                                    try {
                                        Write.smsMediumThershold.setValue(newValue11);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.smsMediumThershold.setValue("619");
                                    gattService.writeCharacteristic(Write.smsMediumThershold);
                                }
                                Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

//                        }
//                    });

                        //==================== sandy Dry ====================
//                    sandydry.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                        String value2= SandydryEdit.getText().toString();
                        soilsandymediumdry.setText("1229");

                        saveData();

//                        if(value2.length()<=4 && !(value2.length()==0)) {
//
//                            if (Integer.valueOf(value2) <= 4095) {


                                byte[] newValue2 = value2.getBytes();
                                System.out.println(newValue2);

                                System.out.println(Arrays.toString(newValue2));
                                Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.smsDryThershold.setValue(newValue2);
                                SandydryEdit.setText("");

                                if (value2 != null) {

                                    byte[] newValue12 = value2.getBytes();

                                    try {
                                        Write.smsDryThershold.setValue(newValue12);
                                        gattService.writeCharacteristic(Write.smsDryThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.smsDryThershold.setValue("1229");
                                    gattService.writeCharacteristic(Write.smsDryThershold);
                                }
                                Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
//                            }
//                            else {
//                                SandydryEdit.setText("1299");
//                            }
//                        }
//                        else {
//                            SandydryEdit.setText("");
//                        }
                    }


                });

            }




                else  if("CLAY"==type[i]){



                    LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(900, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp2.setLayoutDirection(LinearLayout.VERTICAL);
                    lp2.setMarginStart(50);



                    ClaywetEdit= new EditText(getApplicationContext());
                    ClaymediumEdit= new EditText(getApplicationContext());
                    ClaydryEdit= new EditText(getApplicationContext());

                    soilclaywet= new TextView(getApplicationContext());
                    soilclaywet.setTypeface(null, Typeface.BOLD);
                    soilclaymediumwet= new TextView(getApplicationContext());
                    soilclaymediumwet.setTypeface(null, Typeface.BOLD);
                    soilclaymediumdry= new TextView(getApplicationContext());
                    soilclaymediumdry.setTypeface(null, Typeface.BOLD);

                    //============================================global value =======================
                    Write.clayWet=ClaywetEdit.getText().toString();
                    Write.clayMedium=ClaymediumEdit.getText().toString();
                    Write.clayDry= ClaydryEdit.getText().toString();


                    ClaywetEdit.setHint("Wet : 0 to 1024");
                    ClaymediumEdit.setHint("Medium Wet : 1024 - 1638  ");
                    ClaydryEdit.setHint("Medium Dry : 1638 - 2048  ");



                    claywet= new Button(getApplicationContext());
                    claywet.setBackgroundColor(R.drawable.square_button);
                    claywet.setTextColor(Color.WHITE);

                    claydefault= new Button(getApplicationContext());
                    claydefault.setBackgroundColor(Color.parseColor("#40cf68"));
                    claydefault.setTextColor(Color.WHITE);
//                    claydry= new Button(getApplicationContext());
//                    claydry.setBackgroundColor(Color.parseColor("#ffff66"));

                    claywet.setLayoutParams(wetbtn);
                    claydefault.setLayoutParams(defaultbtn);
//                    claydry.setLayoutParams(mediumbtn);
//                    claymedium.setLayoutParams(drybtn);


                    claywet.setText("Send");
                    claydefault.setText("Default");
//                    claymedium.setText("send");
//                    claydry.setText("send");


                    ClaywetEdit.setLayoutParams(lp2);
                    ClaymediumEdit.setLayoutParams(lp2);
                    ClaydryEdit.setLayoutParams(lp2);

                    soilclaywet.setLayoutParams(lp2);
                    soilclaymediumwet.setLayoutParams(lp2);
                    soilclaymediumdry.setLayoutParams(lp2);

                    layout.addView(ClaywetEdit);
                    layoutMedium.addView(ClaymediumEdit);
                    layoutDry.addView(ClaydryEdit);

                    save.addView(soilclaywet);
                    save1.addView(soilclaymediumwet);
                    save2.addView(soilclaymediumdry);

                    layoutbtn.addView(claywet);
                    layoutMediumbtn.addView(claydefault);
//                    layoutDrybtn.addView(claydry);

                    layout.setVisibility(View.VISIBLE);
                    layoutMedium.setVisibility(View.VISIBLE);
                    layoutDry.setVisibility(View.VISIBLE);

                    save.setVisibility(View.VISIBLE);
                    save1.setVisibility(View.VISIBLE);
                    save2.setVisibility(View.VISIBLE);

                    layoutbtn.setVisibility(View.VISIBLE);
                    layoutMediumbtn.setVisibility(View.VISIBLE);
//                    layoutDrybtn.setVisibility(View.VISIBLE);

                    //========================== Button action For Write CharcterStics ========================
                    //==================== clay wet ====================
                    claywet.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            claywet.setBackgroundColor(Color.parseColor("#32CD32"));
                            String value= ClaywetEdit.getText().toString();
                            soilclaywet.setText(ClaywetEdit.getText().toString());
                            saveData01();
                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 4095) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsWetThershold.setValue(newValue);
                                    ClaywetEdit.setText("");

                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsWetThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsWetThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsWetThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsWetThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    ClaywetEdit.setError("Enter valid Thershold");
                                }
                            }
                            else {
                                ClaywetEdit.setError("Enter 4 digit Number ");
                            }
//                        }
//                    });

                    //==================== clay Medium ====================
//                    claymedium.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                            String value1= ClaymediumEdit.getText().toString();
                            soilclaymediumwet.setText(ClaymediumEdit.getText().toString());
                            saveData01();
                            if(value1.length()<=4 && !(value1.length()==0)) {

                                if (Integer.valueOf(value1) <= 4095) {


                                    byte[] newValue1 = value1.getBytes();
                                    System.out.println(newValue1);

                                    System.out.println(Arrays.toString(newValue1));
                                    Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsMediumThershold.setValue(newValue1);
                                    ClaymediumEdit.setText("");

                                    if (value1 != null) {

                                        byte[] newValue11 = value1.getBytes();

                                        try {
                                            Write.smsMediumThershold.setValue(newValue11);
                                            gattService.writeCharacteristic(Write.smsMediumThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsMediumThershold.setValue(value1);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    ClaymediumEdit.setError("Enter valid Thershold");
                                }
                            }
                            else {
                                ClaymediumEdit.setError("Enter 4 digit Number ");
                            }
//                        }
//                    });

                    //==================== clay dry  ====================
//                    claydry.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                            String value2= ClaydryEdit.getText().toString();
                            soilclaymediumdry.setText(ClaydryEdit.getText().toString());
                            saveData01();
                            if(value2.length()<=4 && !(value2.length()==0)) {

                                if (Integer.valueOf(value2) <= 4095) {


                                    byte[] newValue2 = value2.getBytes();
                                    System.out.println(newValue2);

                                    System.out.println(Arrays.toString(newValue2));
                                    Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsDryThershold.setValue(newValue2);
                                    ClaydryEdit.setText("");

                                    if (value2 != null) {

                                        byte[] newValue12 = value2.getBytes();

                                        try {
                                            Write.smsDryThershold.setValue(newValue12);
                                            gattService.writeCharacteristic(Write.smsDryThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsDryThershold.setValue(value2);
                                        gattService.writeCharacteristic(Write.smsDryThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    ClaydryEdit.setError("Enter valid Thershold");
                                }
                            }
                            else {
                                ClaydryEdit.setError("Enter 4 digit Number ");
                            }
                        }
                    });
                    //==========================================================================//
                    loadData01();
                    updateViews01();



                //// Send default value to device

                //==================== clay wet ====================
                claydefault.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        claydefault.setBackgroundColor(Color.parseColor("#00ccff"));
                        String value= ClaywetEdit.getText().toString();
                        soilclaywet.setText("1024");
                        saveData01();

                                byte[] newValue = value.getBytes();
                                System.out.println(newValue);

                                System.out.println(Arrays.toString(newValue));
                                Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.smsWetThershold.setValue(newValue);
                                ClaywetEdit.setText("");

                                if (value != null) {

                                    byte[] newValue1 = value.getBytes();

                                    try {
                                        Write.smsWetThershold.setValue(newValue1);
                                        gattService.writeCharacteristic(Write.smsWetThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.smsWetThershold.setValue("1024");
                                    gattService.writeCharacteristic(Write.smsWetThershold);
                                }
                                Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

//                        }
//                    });

                        //==================== clay Medium ====================
//                    claymedium.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                        String value1= ClaymediumEdit.getText().toString();
                        soilclaymediumwet.setText("1638");
                        saveData01();

                                byte[] newValue1 = value1.getBytes();
                                System.out.println(newValue1);

                                System.out.println(Arrays.toString(newValue1));
                                Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.smsMediumThershold.setValue(newValue1);
                                ClaymediumEdit.setText("");

                                if (value1 != null) {

                                    byte[] newValue11 = value1.getBytes();

                                    try {
                                        Write.smsMediumThershold.setValue(newValue11);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.smsMediumThershold.setValue("1638");
                                    gattService.writeCharacteristic(Write.smsMediumThershold);
                                }
                                Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
//                        }
//                    });

                        //==================== clay dry  ====================
//                    claydry.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                        String value2= ClaydryEdit.getText().toString();
                        soilclaymediumdry.setText("2048");
                        saveData01();
//                        if(value2.length()<=4 && !(value2.length()==0)) {
//
//                            if (Integer.valueOf(value2) <= 4095) {


                                byte[] newValue2 = value2.getBytes();
                                System.out.println(newValue2);

                                System.out.println(Arrays.toString(newValue2));
                                Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.smsDryThershold.setValue(newValue2);
                                ClaydryEdit.setText("");

                                if (value2 != null) {

                                    byte[] newValue12 = value2.getBytes();

                                    try {
                                        Write.smsDryThershold.setValue(newValue12);
                                        gattService.writeCharacteristic(Write.smsDryThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.smsDryThershold.setValue("2048");
                                    gattService.writeCharacteristic(Write.smsDryThershold);
                                }
                                Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
//                            }
//                            else {
//                                ClaydryEdit.setError("Enter valid Thershold");
//                            }
//                        }
//                        else {
//                            ClaydryEdit.setError("Enter 4 digit Number ");
//                        }
                    }
                });
                //==========================================================================//

            }

                else {
                    LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(900, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp3.setLayoutDirection(LinearLayout.VERTICAL);
                    lp3.setMarginStart(50);



                    SiltwetEdit= new EditText(getApplicationContext());
                    SiltmediumEdit= new EditText(getApplicationContext());
                    SiltdryEdit= new EditText(getApplicationContext());


                    soilsiltwet= new TextView(getApplicationContext());
                    soilsiltwet.setTypeface(null, Typeface.BOLD);
                    soilsiltmediumwet= new TextView(getApplicationContext());
                    soilsiltmediumwet.setTypeface(null, Typeface.BOLD);
                    soilsiltmediumdry= new TextView(getApplicationContext());
                    soilsiltmediumdry.setTypeface(null, Typeface.BOLD);

                    SiltwetEdit.setHint("Wet : 0 - 491");
                    SiltmediumEdit.setHint("Medium Wet: 491 - 983");
                    SiltdryEdit.setHint("Medium Dry : 983 - 1638");


                    //=============================global value ==========================================
                    Write.siltWet=SiltwetEdit.getText().toString();
                    Write.siltMedium=SiltmediumEdit.getText().toString();
                    Write.siltDry=SiltdryEdit.getText().toString();

                    siltwet= new Button(getApplicationContext());
                    siltwet.setBackgroundColor(R.drawable.square_button);
                    siltwet.setTextColor(Color.WHITE);

                    siltdefault= new Button(getApplicationContext());
                    siltdefault.setBackgroundColor(Color.parseColor("#40cf68"));
                    siltdefault.setTextColor(Color.WHITE);
//                    siltdry= new Button(getApplicationContext());
//                    siltdry.setBackgroundColor(Color.parseColor("#ffff66"));


                    siltwet.setLayoutParams(wetbtn);
                    siltdefault.setLayoutParams(defaultbtn);
//                    siltdry.setLayoutParams(drybtn);


                    siltwet.setText("Send");
                    siltdefault.setText("Default");
//                    siltdry.setText("send");


                    SiltwetEdit.setLayoutParams(lp3);
                    SiltdryEdit.setLayoutParams(lp3);
                    SiltmediumEdit.setLayoutParams(lp3);


                    soilsiltwet.setLayoutParams(lp3);
                    soilsiltmediumwet.setLayoutParams(lp3);
                    soilsiltmediumdry.setLayoutParams(lp3);


                    layout.addView(SiltwetEdit);
                    layoutMedium.addView(SiltmediumEdit);
                    layoutDry.addView(SiltdryEdit);

                    save.addView(soilsiltwet);
                    save1.addView(soilsiltmediumwet);
                    save2.addView(soilsiltmediumdry);

                    layoutbtn.addView(siltwet);
                    layoutMediumbtn.addView(siltdefault);
//                    layoutDrybtn.addView(siltdry);

                    layout.setVisibility(View.VISIBLE);
                    layoutMedium.setVisibility(View.VISIBLE);
                    layoutDry.setVisibility(View.VISIBLE);

                    save.setVisibility(View.VISIBLE);
                    save1.setVisibility(View.VISIBLE);
                    save2.setVisibility(View.VISIBLE);

                    layoutbtn.setVisibility(View.VISIBLE);
                    layoutMediumbtn.setVisibility(View.VISIBLE);
//                    layoutDrybtn.setVisibility(View.VISIBLE);

                    //========================== Button action For Write CharcterStics ========================
                    //==================== silt wet ====================
                    siltwet.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            siltwet.setBackgroundColor(Color.parseColor("#32CD32"));
                            String value= SiltwetEdit.getText().toString();
                            soilsiltwet.setText(SiltwetEdit.getText().toString());
                            saveData02();
                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 4095) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsWetThershold.setValue(newValue);
                                    SiltwetEdit.setText("");

                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsWetThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsWetThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsWetThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsWetThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SiltwetEdit.setError("Enter valid Thershold");
                                }
                            }
                            else {
                                SiltwetEdit.setError("Enter 4 digit Number ");
                            }
//                        }
//                    });

                    //==================== silt Medium ====================
//                    siltmedium.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                            String value1= SiltmediumEdit.getText().toString();
                            soilsiltmediumwet.setText(SiltmediumEdit.getText().toString());
                            saveData02();
                            if(value1.length()<=4 && !(value1.length()==0)) {

                                if (Integer.valueOf(value1) <= 4095) {


                                    byte[] newValue1 = value1.getBytes();
                                    System.out.println(newValue1);

                                    System.out.println(Arrays.toString(newValue1));
                                    Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsMediumThershold.setValue(newValue1);
                                    SiltmediumEdit.setText("");

                                    if (value1 != null) {

                                        byte[] newValue11 = value1.getBytes();

                                        try {
                                            Write.smsMediumThershold.setValue(newValue11);
                                            gattService.writeCharacteristic(Write.smsMediumThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsMediumThershold.setValue(value1);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SiltmediumEdit.setError("Enter valid Thershold");
                                }
                            }
                            else {
                                SiltmediumEdit.setError("Enter 4 digit Number ");
                            }
//                        }
//                    });

                    //==================== silt dry  ====================
//                    siltdry.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                            String value2= SiltdryEdit.getText().toString();
                            soilsiltmediumdry.setText(SiltdryEdit.getText().toString());
                            saveData02();
                            if(value2.length()<=4 && !(value2.length()==0)) {

                                if (Integer.valueOf(value2) <= 4095) {


                                    byte[] newValue2 = value2.getBytes();
                                    System.out.println(newValue2);

                                    System.out.println(Arrays.toString(newValue2));
                                    Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsDryThershold.setValue(newValue2);
                                    SiltdryEdit.setText("");

                                    if (value2 != null) {

                                        byte[] newValue12 = value2.getBytes();

                                        try {
                                            Write.smsDryThershold.setValue(newValue12);
                                            gattService.writeCharacteristic(Write.smsDryThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsDryThershold.setValue(value2);
                                        gattService.writeCharacteristic(Write.smsDryThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SiltdryEdit.setError("Enter valid Thershold");
                                }
                            }
                            else {
                                SiltdryEdit.setError("Enter 4 digit Number ");
                            }
                        }
                    });
                //====================================================//

                    loadData02();
                    updateViews02();

                    ///Default value send button
                    //==================== silt wet ====================
                    siltdefault.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            siltdefault.setBackgroundColor(Color.parseColor("#00ccff"));
                            String value= SiltwetEdit.getText().toString();
                            soilsiltwet.setText("491");
                            saveData02();

                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsWetThershold.setValue(newValue);
                                    SiltwetEdit.setText("");

                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsWetThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsWetThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsWetThershold.setValue("491");
                                        gattService.writeCharacteristic(Write.smsWetThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

//                        }
//                    });

                            //==================== silt Medium ====================
//                    siltmedium.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                            String value1= SiltmediumEdit.getText().toString();
                            soilsiltmediumwet.setText("983");
                            saveData02();



                                    byte[] newValue1 = value1.getBytes();
                                    System.out.println(newValue1);

                                    System.out.println(Arrays.toString(newValue1));
                                    Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsMediumThershold.setValue(newValue1);
                                    SiltmediumEdit.setText("");

                                    if (value1 != null) {

                                        byte[] newValue11 = value1.getBytes();

                                        try {
                                            Write.smsMediumThershold.setValue(newValue11);
                                            gattService.writeCharacteristic(Write.smsMediumThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsMediumThershold.setValue("983");
                                        gattService.writeCharacteristic(Write.smsMediumThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

//                        }
//                    });

                            //==================== silt dry  ====================
//                    siltdry.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View v) {
                            String value2= SiltdryEdit.getText().toString();
                            soilsiltmediumdry.setText("1638");
                            saveData02();

                                    byte[] newValue2 = value2.getBytes();
                                    System.out.println(newValue2);

                                    System.out.println(Arrays.toString(newValue2));
                                    Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsDryThershold.setValue(newValue2);
                                    SiltdryEdit.setText("");

                                    if (value2 != null) {

                                        byte[] newValue12 = value2.getBytes();

                                        try {
                                            Write.smsDryThershold.setValue(newValue12);
                                            gattService.writeCharacteristic(Write.smsDryThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsDryThershold.setValue("1638");
                                        gattService.writeCharacteristic(Write.smsDryThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

                        }
                    });
                    //====================================================//


                }


                //==========================LowLevel BAttery ========================

                battLow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String value = batLowtext.getText().toString();
                        if (value.length() <= 4 && !(value.length()==0)) {
                            if (Integer.valueOf(value) <= 4095) {

                                byte[] newValue = value.getBytes();
                                System.out.println(newValue);

                                System.out.println(Arrays.toString(newValue));
                                Write.batLowThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.batLowThershold.setValue(newValue);
                                batLowtext.setText("");
                                //    writeCharacteristic(Write.batLowThershold);
                                if (value != null) {

                                    byte[] newValue1 = value.getBytes();

                                    try {
                                        Write.batLowThershold.setValue(newValue1);
                                        gattService.writeCharacteristic(Write.batLowThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.batLowThershold.setValue(value);
                                    gattService.writeCharacteristic(Write.batLowThershold);
                                }
                                Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

                            }
                            else {
                                batLowtext.setError("Enter valid Thershold");
                            }
                        }
                        else {
                            batLowtext.setError("Enter 4 digit Number ");
                        }
                    }
                });



                //=====================================================
                //==========================HighLevel BAttery ========================

                bttnHigh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String value = textHigh.getText().toString();
                        if (value.length() <= 4 && !(value.length() == 0)) {
                            if (Integer.valueOf(value) <= 4095) {

                                byte[] newValue = value.getBytes();
                                System.out.println(newValue);

                                System.out.println(Arrays.toString(newValue));
                                Write.batHighThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                Write.batHighThershold.setValue(newValue);
                                textHigh.setText("");
                                //    writeCharacteristic(Write.batLowThershold);
                                if (value != null) {

                                    byte[] newValue1 = value.getBytes();

                                    try {
                                        Write.batHighThershold.setValue(newValue1);
                                        gattService.writeCharacteristic(Write.batHighThershold);

                                    } catch (Exception e) {
                                        Log.e("Service", "null" + e);
                                    }
                                } else {
                                    Write.batHighThershold.setValue(value);
                                    gattService.writeCharacteristic(Write.batHighThershold);
                                }
                                Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                            }
                            else {
                                textHigh.setError("Enter valid Thershold");
                            }
                        }
                        else { textHigh.setError("Enter 4 digit Number ");
                        }}
                });
                //=====================================================
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void checkValidation() {
        if (SandywetEdit.length() <=1 && SandymediumEdit.length() <=1 && SandydryEdit.length() <=1 && ! (SandywetEdit.length() ==0) && !(SandymediumEdit.length()==0) && ! (SandydryEdit.length() ==0) )  {
            sandywet.setEnabled(true);
        } else {
            sandywet.setEnabled(true);
        }
    }

    // save the input data
    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        SharedPreferences.Editor editor1 = sharedPreferences.edit();
        SharedPreferences.Editor editor2 = sharedPreferences.edit();
        SharedPreferences.Editor editor3 = sharedPreferences.edit();
        editor.putString(SANDYWETDATASAVE, soilsandywet.getText().toString());
        editor1.putString(SANDYMEDIUMWETDATASAVE, soilsandymediumwet.getText().toString());
        editor2.putString(SANDYMEDIUMDRYDATASAVE, soilsandymediumdry.getText().toString());
//      editor2.putString(TEXT3, save3.getText().toString());
        editor.apply();
        editor1.apply();
        editor2.apply();
//        editor3.apply();
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
    }
    //Toast.makeText(getApplicationContext(),"You are Selected:" +type[i],Toast.LENGTH_SHORT).show();
    //load input data in app whenever open the app tha last input data will we displayed in app
    public void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        sandywetdatasave = sharedPreferences.getString(SANDYWETDATASAVE, "");
        sandymediumwetdatasave = sharedPreferences.getString(SANDYMEDIUMWETDATASAVE, "");
        sandymediumdrydatasave = sharedPreferences.getString(SANDYMEDIUMDRYDATASAVE, "");
//        text3 = sharedPreferences.getString(TEXT3, "");
    }

    ///update input data in app
    public void updateViews() {
//        save3.setText("Soil Type  : "+text2);
         soilsandywet.setText("Wet           : "+sandywetdatasave);
        soilsandymediumwet.setText("Medium Wet : "+sandymediumwetdatasave);
        soilsandymediumdry.setText("Medium Dry : "+sandymediumdrydatasave);
    }

    ///save 2

    // save the input data
    private void saveData01() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS01, MODE_PRIVATE);
        SharedPreferences.Editor editors = sharedPreferences.edit();
        SharedPreferences.Editor editors1 = sharedPreferences.edit();
        SharedPreferences.Editor editors2 = sharedPreferences.edit();
        SharedPreferences.Editor editor3 = sharedPreferences.edit();
        editors.putString(CLAYWETDATASAVE, soilclaywet.getText().toString());
        editors1.putString(CLAYMEDIUMWETDATASAVE, soilclaymediumwet.getText().toString());
        editors2.putString(CLAYMEDIUMDRYDATASAVE, soilclaymediumdry.getText().toString());
//      editor2.putString(TEXT3, save3.getText().toString());
        editors.apply();
        editors1.apply();
        editors2.apply();
//        editor3.apply();
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
    }
    //Toast.makeText(getApplicationContext(),"You are Selected:" +type[i],Toast.LENGTH_SHORT).show();
    //load input data in app whenever open the app tha last input data will we displayed in app
    public void loadData01() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS01, MODE_PRIVATE);
        claywetdatasave = sharedPreferences.getString(CLAYWETDATASAVE, "");
        claymediumwetdatasave = sharedPreferences.getString(CLAYMEDIUMWETDATASAVE, "");
        claymediumdrydatasave = sharedPreferences.getString(CLAYMEDIUMDRYDATASAVE, "");
//        text3 = sharedPreferences.getString(TEXT3, "");
    }

    ///update input data in app
    public void updateViews01() {
//        save3.setText("Soil Type  : "+text2);
        soilclaywet.setText("Wet              : "+claywetdatasave);
        soilclaymediumwet.setText("Medium Wet : "+claymediumwetdatasave);
        soilclaymediumdry.setText("Medium Dry : "+claymediumdrydatasave);

    }


    ///save 3

    // save the input data
    private void saveData02() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS02, MODE_PRIVATE);
        SharedPreferences.Editor editors01 = sharedPreferences.edit();
        SharedPreferences.Editor editors02 = sharedPreferences.edit();
        SharedPreferences.Editor editors03 = sharedPreferences.edit();
        SharedPreferences.Editor editor3 = sharedPreferences.edit();
        editors01.putString(SILTWETDATASAVE, soilsiltwet.getText().toString());
        editors02.putString(SILTMEDIUMWETDATASAVE, soilsiltmediumwet.getText().toString());
        editors03.putString(SILTMEDIUMDRYDATASAVE, soilsiltmediumdry.getText().toString());
//      editor2.putString(TEXT3, save3.getText().toString());
        editors01.apply();
        editors02.apply();
        editors03.apply();
//        editor3.apply();
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
    }
    //Toast.makeText(getApplicationContext(),"You are Selected:" +type[i],Toast.LENGTH_SHORT).show();
    //load input data in app whenever open the app tha last input data will we displayed in app
    public void loadData02() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS02, MODE_PRIVATE);
        siltwetdatasave = sharedPreferences.getString(SILTWETDATASAVE, "");
        siltmediumwetdatasave = sharedPreferences.getString(SILTMEDIUMWETDATASAVE, "");
        siltmediumdrydatasave = sharedPreferences.getString(SILTMEDIUMDRYDATASAVE, "");
//        text3 = sharedPreferences.getString(TEXT3, "");
    }

    ///update input data in app
    public void updateViews02() {
//        save3.setText("Soil Type  : "+text2);
        soilsiltwet.setText("Wet           : "+siltwetdatasave);
        soilsiltmediumwet.setText("Medium Wet : "+siltmediumwetdatasave);
        soilsiltmediumdry.setText("Medium Dry : "+siltmediumdrydatasave);

    }




    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.activity_help, viewGroup, false);



        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }


}