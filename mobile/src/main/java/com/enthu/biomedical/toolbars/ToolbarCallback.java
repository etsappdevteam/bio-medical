package com.enthu.biomedical.toolbars;

import com.enthu.biomedical.utils.FilterDeviceParams;

public interface ToolbarCallback {
    void close();

    void submit(FilterDeviceParams filterDeviceParams, boolean close);

}
